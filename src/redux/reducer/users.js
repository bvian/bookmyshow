import { ADD_CURRENT_USER, REMOVE_CURRENT_USER } from "../actions/userAction";

const initialState = {
    currentUser: null
}

export default function actionProduct(state = initialState, action) {
    switch (action.type) {

        case ADD_CURRENT_USER:
            return {
                ...state,
                currentUser: action.payload
            }

        case REMOVE_CURRENT_USER:
            return {
                ...state,
                currentUser: null
            }

        default: {
            return state;
        }
    }
}