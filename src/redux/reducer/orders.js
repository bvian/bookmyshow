import { ADD_ORDER, ALL_ORDERS } from "../actions/ordersAction";

const initialState = {
    list: null
}

export default function actionMovies(state = initialState, action) {
    switch (action.type) {

        case ADD_ORDER:
            return {
                list: [
                    ...state.list,
                    action.payload
                ]
            }

        case ALL_ORDERS:
            return {
                list: action.payload
            }

        default: {
            return state;
        }
    }
}