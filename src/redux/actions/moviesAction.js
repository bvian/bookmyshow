import axios from "axios";

export const ERROR = 'ERROR';
export const MOVIES = 'MOVIES';
export const CINEMAS = 'CINEMAS';
export const UPDATE_CINEMA = 'UPDTE_CINEMA';

export const showErrors = (errorMessage) => ({
    type: ERROR,
    payload: errorMessage
});

export const getMovies = (movies) => ({
    type: MOVIES,
    payload: movies,
});

export const getCinemas = (cinemas) => ({
    type: CINEMAS,
    payload: cinemas,
});

export const updateCinema = (cinema) => ({
    type: UPDATE_CINEMA,
    payload: cinema
});

export const fetchMovies = () => (dispatch) => {
    axios.get('https://bookmyshowapi.vercel.app/api/movies')
        .then((response) => {
            dispatch(getMovies(response.data));
        })
        .catch((err) => {
            console.error(err);
            const errorMessage = "Cannot Fetch Movies from API, " + err.message;
            dispatch(showErrors(errorMessage))
        });
}

export const fetchCinemas = () => (dispatch) => {
    axios.get('https://bookmyshowapi.vercel.app/api/cinemahalls')
        .then((response) => {
            dispatch(getCinemas(response.data));
        })
        .catch((err) => {
            console.error(err);
            const errorMessage = "Cannot Fetch Cinemas from API, " + err.message;
            dispatch(showErrors(errorMessage))
        });
}

export const updateCinemaToApi = (cinema) => (dispatch) => {
    axios.put(`https://bookmyshowapi.vercel.app/api/cinemahalls/${cinema.id}`, cinema)
        .then((response) => {
            dispatch(updateCinema(response.data));
        })
        .catch((err) => {
            console.error(err);
            const errorMessage = "cannot PUT cinema to API, " + err.message;
            dispatch(showErrors(errorMessage));
        });
}