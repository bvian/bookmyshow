import axios from "axios";

import { showErrors } from "./moviesAction";

export const ERROR = 'ERROR';
export const ALL_ORDERS = 'ALL_ORDERS';
export const ADD_ORDER = 'ADD_ORDER';

export const addOrder = (order) => ({
    type: ADD_ORDER,
    payload: order,
});

export const allOrders = (orders) => ({
    type: ALL_ORDERS,
    payload: orders
})

export const fetchOrders = (currentUser) => (dispatch) => {
    axios.get('https://bookmyshowapi.vercel.app/api/orders')
        .then((response) => {
            const orders = response.data.filter((order) => {
                return order.userId === currentUser?.id;
            });
            dispatch(allOrders(orders));
        })
        .catch((err) => {
            console.error(err);
            const errorMessage = "Cannot Fetch Orders from API, " + err.message;
            dispatch(showErrors(errorMessage))
        });
}

export const addNewOrder = (order) => (dispatch) => {
    axios.post('https://bookmyshowapi.vercel.app/api/orders', order)
        .then((response) => {
            dispatch(addOrder(response.data));
        })
        .catch((err) => {
            console.error(err);
            const errorMessage = "Cannot Post Order in API, " + err.message;
            dispatch(showErrors(errorMessage))
        });
}