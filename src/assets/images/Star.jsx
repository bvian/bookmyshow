import React from "react";

function Icon() {
    return (
        <svg width="32" height="32">
            <use href="/chunks/icons/common-icons-bdce040e.svg#icon-star"></use>
        </svg>
    );
}

export default Icon;
