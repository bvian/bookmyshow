import React, { Component } from 'react';
import { connect } from 'react-redux';

import './PremieresMovies.css';
class PremieresMovies extends Component {
    render() {
        return (
            <div className='w-100 premieres-movies'>
                <div className='premieres-movies-contain'>
                    <div>
                        <h4 className='head text-white'>Premieres</h4>
                        <p>Brand new releases every Friday</p>
                    </div>
                    <div className='d-flex card-premiere'>
                        {
                            this.props.premieresMovies.map((movie) => {
                                return (
                                    <div key={movie.id} className="card premiere-movie-card">
                                        <img className="card-img-top" src={movie.image} alt="" style={{ width: '100%', height: '75%' }} />
                                        <h6 className="card-title">{movie.title}</h6>
                                        <p>{movie.language}</p>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        premieresMovies: state.events.premieresMovies
    }
}

export default connect(mapStateToProps)(PremieresMovies);