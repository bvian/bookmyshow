import React, { Component } from 'react'
import { Link } from 'react-router-dom';

import './OtherAllEvents.css';

export class OtherAllEvents extends Component {
  render() {
    const { title, events } = this.props;
    const divId = '#' + title;
    return (
      <div className='carousel carousel-style'>
        <div className='d-flex justify-content-between align-items-center my-3'>
          <h4 className='head'>{title}</h4>
          <p className='see-all'>{'See All >'}</p>
        </div>
        <div id={title} className="carousel slide w-100" data-wrap="false" data-interval="false">
          <div className="carousel-inner d-flex">
            <div className="carousel-item active">
              <div className='d-flex movies-contain'>
                {
                  events[0].map((movie) => {
                    const eventUrl = "/events/" + movie.title.toLowerCase()
                      .replaceAll('-', '')
                      .replaceAll('  ', ' ')
                      .replaceAll(' ', '-');
                    return (
                      <Link to={eventUrl} key={movie.id}>
                        <div className="card movie-card" style={{ width: "14em", height: "32em" }}>
                          <img className="card-img-top" src={movie.image} alt="" style={{ width: '100%', height: '75%' }} />
                          <h6 className="card-title">{movie.title}</h6>
                          <p>{movie.description}</p>
                          {movie.distance && <p>{movie.distance}</p>}
                        </div>
                      </Link>
                    )
                  })
                }
              </div>
            </div>
            <div className="carousel-item">
              <div className='d-flex movies-contain'>
                {
                  events[1].map((movie) => {
                    const movieUrl = "/events/" + movie.title.toLowerCase()
                      .replaceAll('-', '')
                      .replaceAll('  ', ' ')
                      .replaceAll(' ', '-');

                    return (
                      <Link to={movieUrl} key={movie.id}>
                        <div className="card movie-card" style={{ width: "14em", height: "32em" }}>
                          <img className="card-img-top" src={movie.image} alt="" style={{ width: '100%', height: '75%' }} />
                          <h6 className="card-title">{movie.title}</h6>
                          <p>{movie.description}</p>
                          {movie.distance && <p>{movie.distance}</p>}
                        </div>
                      </Link>
                    )
                  })
                }
              </div>
            </div>
          </div>
          <a className="carousel-control-prev" href={divId} role="button" data-slide="prev">
            <span className="carousel-control-prev-icon arrowTop" aria-hidden="true"></span>
            <span className="sr-only">Previous</span>
          </a>
          <a className="carousel-control-next" href={divId} role="button" data-slide="next">
            <span className="carousel-control-next-icon arrowTop" aria-hidden="true"></span>
            <span className="sr-only">Next</span>
          </a>
        </div>

      </div>
    )
  }
}

export default OtherAllEvents;