import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";

class TermsAndCondition extends Component {

    geturl() {
        let url = window.location.href.split('/').slice(3);
        // url.remove('');
        if (url.length === 4) {
            return "/" + url.join('/') + this.props.cinema.id + "/" + this.props.time.replaceAll(" ", "-");
        } else {
            return "/" + url.join('/') + "/" + this.props.cinema.id + "/" + this.props.time.replaceAll(" ", "-");
        }
    }

    handleClick = () => {

        setTimeout(() => {
            window.location.reload();
        }, 10);
    }

    render() {
        const newUrl = this.geturl();

        return (
            <div className="modal fade" id={"termsCondition" + this.props.index + this.props.time} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content px-3 py-1">
                        <div className='d-flex justify-content-between text-dark py-4 w-100'>
                            <h5 style={{ textAlign: "center", flex: "10" }}>Terms & Condition</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" style={{ flex: "1" }}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div style={{ textAlign: "left", fontSize: "1em", color: "#8B8F93" }}>
                            <p>1.Wearing an N-95 mask and following other Covid guidelines are mandatory (As per the directions from your local authorities). . </p>
                            <p>2. Please pick up your tickets at least 20 mins before showtime to avoid rush at the counter.</p>
                            <p>3. Outside food & beverages are not allowed inside the cinema premises.</p>
                            <p>4. Ticket is compulsory for children of 3 years & above.</p>
                            <p>5. Ticket for A rated movie should not be purchased for people under 18 years of age. There won`t be a refund for tickets booked in such cases.</p>
                            <p>6. Handbags, Laptops/Tabs, cameras and all other electronic items are not allowed inside cinema premises.</p>
                            <p>7. Smoking is strictly not permitted inside the cinema premises. Cigarettes/lighters/matchsticks/Gutkha/Pan masala etc. will not be allowed.</p>
                            <p>8. Cinema reserves the Right of Admission.</p>
                            <p>9. People under the influence of Alcohol/Drugs will not be allowed inside the cinema premise.</p>
                            <p>10. Tickets once purchased cannot be exchanged or adjusted/transferred for any other shows.</p>
                            <p>11. Screenshots or forwarded messages will not be accepted during the entry at the Cinema.</p>

                        </div>
                        <div className="w-100 d-flex justify-content-center align-items-center py-4" style={{ gap: '2em' }}>
                            <button data-dismiss="modal" style={{ outline: "none", border: "1px solid #f84464", backgroundColor: "white", color: "#f84464", width: "15em", height: "3em", borderRadius: "0.7em", cursor: "pointer" }}>
                                Cancel
                            </button>
                            <Link to={newUrl}>
                                <button onClick={this.handleClick} style={{ outline: "none", color: "white", backgroundColor: "#f84464", width: "15em", height: "3em", borderRadius: "0.7em", cursor: "pointer" }}>
                                    Accept
                                </button>
                            </Link>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        currentMovieDetails: (movieDetails) => dispatch({
            type: "CURRENT_MOVIE_DETAILS",
            payload: movieDetails
        })
    }
}

export default connect(null, mapDispatchToProps)(TermsAndCondition);