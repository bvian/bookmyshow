import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class LanguagePopUp extends Component {

    handleClick = () => {
        setTimeout(() => {
            window.location.reload();
        }, 10);
    }

    render() {
        return (
            <div className="modal fade" id="languageModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content py-2">

                        <div className="p-3 w-100 modal-body">
                            <div className='d-flex justify-content-between text-dark'>
                                <span>{this.props.movie?.title}</span>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className='text-dark font-weight-bold'>Select language and format</div>
                            <div className='py-3 d-flex flex-wrap'>
                                {
                                    this.props.movie?.language?.map((lang) => {
                                        let movieTicketUrl = "/buytickets/" + this.props.movie.title.toLowerCase()
                                            .replaceAll('-', '')
                                            .replaceAll('  ', ' ')
                                            .replaceAll(' ', '-');
                                        movieTicketUrl += "/" + lang.toLowerCase();
                                        return (
                                            <Link to={movieTicketUrl} key={lang} onClick={this.handleClick}>
                                                <div style={{ border: "1px solid lightgrey", margin: "0.6em", padding: "0.6em 1em", color: '#f84464', borderRadius: "2em", cursor: "pointer" }}>
                                                    {lang}
                                                </div>
                                            </Link>
                                        );
                                    })
                                }
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default LanguagePopUp;