import React, { Component } from 'react';
import { connect } from 'react-redux';
import OtherAllEvents from './OtherAllEvents';

class AllEvents extends Component {
    render() {
        return (
            <div>
                {this.props.allEvents.map((allEvent) => {
                    return <>
                        {
                            Object.entries(allEvent).map((event) => {
                                return <OtherAllEvents title={event[0]} events={event[1]} />
                            })
                        }
                    </>
                })
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        allEvents: state.events.list
    }
}

export default connect(mapStateToProps)(AllEvents);