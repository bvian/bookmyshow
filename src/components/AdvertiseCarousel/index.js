import React, { Component } from 'react';

import './AdvertiseCarousel.css';

export class AdvertiseCarousel extends Component {
    render() {
        return (
            <div className='advertise-carousel my-2'>
                <div id="advertiseCarousel" className="carousel slide" data-ride="carousel" data-interval="3000">
                    <ol className="carousel-indicators">
                        <li data-target="#advertiseCarousel" data-slide-to="0" className="active"></li>
                        <li data-target="#advertiseCarousel" data-slide-to="1"></li>
                        <li data-target="#advertiseCarousel" data-slide-to="2"></li>
                    </ol>
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <div className='d-flex justify-content-center'>
                                <img src="https://assets-in.bmscdn.com/promotions/cms/creatives/1680166822388_giftcardweb.jpg" className="d-block w-70 mx-1" alt="..." />
                                <img src="https://assets-in.bmscdn.com/promotions/cms/creatives/1681123745909_webbannernpa.jpg" className="d-block w-70 mx-1" alt="..." />
                                <img src="https://assets-in.bmscdn.com/promotions/cms/creatives/1682326668300_raftarindiadesktop.jpg" className="d-block w-70 mx-1" alt="..." />
                            </div>
                        </div>
                        <div className="carousel-item">
                            <div className='d-flex justify-content-center'>
                                <img src="https://assets-in.bmscdn.com/promotions/cms/creatives/1681123745909_webbannernpa.jpg" className="d-block w-70 mx-1" alt="..." />
                                <img src="https://assets-in.bmscdn.com/promotions/cms/creatives/1682326668300_raftarindiadesktop.jpg" className="d-block w-70 mx-1" alt="..." />
                                <img src="https://assets-in.bmscdn.com/promotions/cms/creatives/1680166822388_giftcardweb.jpg" className="d-block w-70 mx-1" alt="..." />
                            </div>
                        </div>
                        <div className="carousel-item">
                            <div className='d-flex justify-content-center'>
                                <img src="https://assets-in.bmscdn.com/promotions/cms/creatives/1682326668300_raftarindiadesktop.jpg" className="d-block w-70 mx-1" alt="..." />
                                <img src="https://assets-in.bmscdn.com/promotions/cms/creatives/1680166822388_giftcardweb.jpg" className="d-block w-100" alt="..." />
                                <img src="https://assets-in.bmscdn.com/promotions/cms/creatives/1681123745909_webbannernpa.jpg" className="d-block w-70 mx-1" alt="..." />
                            </div>
                        </div>
                    </div>
                    <a className="carousel-control-prev" href="#advertiseCarousel" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#advertiseCarousel" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div>
            </div>
        );
    }
}

export default AdvertiseCarousel;