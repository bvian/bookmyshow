import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addUsers } from '../../redux/actions/userAction';

import './Signin.css';

class Signin extends Component {

    constructor(props) {
        super(props);

        this.state = {
            mobileNumber: '',
            showButton: false
        }
    }

    handleChange = (event) => {
        event.preventDefault();
        const number = String(event.target.value);
        if (number.length <= 10) {
            this.setState({
                mobileNumber: event.target.value
            });
            if (number.length === 10) {
                this.setState({
                    showButton: true
                });
            } else {
                this.setState({
                    showButton: false
                })
            }
        } else {
            return;
        }
    }

    handleClick = () => {
        const userData = {
            name: 'Guest',
            mobileNumber: this.state.mobileNumber,
        };

        this.props.addUsers(userData);
    }

    render() {
        return (
            <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">

                        <div className="w-100 modal-body d-flex flex-column justify-content-center align-items-center signin-contain" style={{ gap: '2em' }}>
                            <div className='d-flex align-items-center justify-content-end p-1 w-100' style={{ gap: "11em" }}>
                                <h5 className='text-dark'>Get Started</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className='d-flex justify-content-start p-2' style={{ border: "1px solid black", gap: "5em", width: '80%' }}>
                                <img className="align-self-start" src="https://in.bmscdn.com/webin/common/icons/googlelogo.svg" alt="" />
                                <span className='align-self-center text-dark'>Continue with Google</span>
                            </div>
                            <div className='d-flex justify-content-start p-2' style={{ border: "1px solid black", gap: "5em", width: '80%' }}>
                                <img className="align-self-start" src="https://in.bmscdn.com/webin/common/icons/email.svg" alt="" />
                                <span className='align-self-center text-dark'>Continue with Email</span>
                            </div>
                            <div className='d-flex justify-content-start p-2' style={{ border: "1px solid black", gap: "5em", width: '80%' }}>
                                <img className="align-self-start" src="https://static.vecteezy.com/system/resources/thumbnails/002/520/838/small/apple-logo-black-isolated-on-transparent-background-free-vector.jpg" alt="" />
                                <span className='align-self-center text-dark'>Continue with Apple</span>
                            </div>

                            <span className='text-dark'>OR</span>

                            <div className='d-flex justify-content-center align-items-center text-dark input-contain' style={{ gap: '0.3em' }}>
                                <img src="https://in.bmscdn.com/webin/common/icons/indianflag.svg" alt="" />
                                +91
                                <input type="number" value={this.state.mobileNumber} placeholder='Continue with mobile number' onChange={this.handleChange} style={{ height: "10%", width: '80%', fontSize: '1em', padding: "0.4em", borderBottom: "1px solid black" }} />
                            </div>
                            {!this.state.showButton &&
                                <div style={{ marginTop: '5em', paddingBottom: '2em', fontSize: '0.8em', color: 'black' }}>
                                    I agree to the <span style={{ textDecoration: "underline" }}>Terms & Conditions</span> & <span style={{ textDecoration: "underline" }}>Privacy Policy</span>
                                </div>
                            }
                            {this.state.showButton &&
                                <button id="signin-continue" onClick={this.handleClick} data-dismiss="modal">Continue</button>
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addUsers: (userData) => {
            dispatch(addUsers(userData));
        }
    }
}

export default connect(null, mapDispatchToProps)(Signin);