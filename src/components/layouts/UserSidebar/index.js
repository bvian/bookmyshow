import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { removeUser } from '../../../redux/actions/userAction';

import './UserSidebar.css';

class UserSidebar extends Component {

    onClickSignOut = () => {
        this.props.logout();
    }

    render() {
        return (
            <div className='bg-light user-side-bar'>
                {this.props.isUser &&
                    <>
                        <div className='d-flex px-3 justify-content-between align-items-center nav'>
                            <div>
                                <h5 className='text-light'>Hey!</h5>
                                <small>Edit Profile</small>
                            </div>
                            <img src="https://in.bmscdn.com/m6/images/my-profile/bms-user.png" alt="" />
                        </div>
                        <div className='d-flex px-2 align-items-center nav2 text-dark'>
                            <div className='d-flex justify-content-center align-items-center info'>
                                i
                            </div>
                            <div className='text-contain'>
                                <p>Get tickets on Email!</p>
                                <p>Add your Email Address</p>
                            </div>
                        </div>
                        <div className='d-flex align-items-center justify-content-center sign-out-button' onClick={this.onClickSignOut}>Sign out</div>
                    </>
                }
                {!this.props.isUser &&
                    <>
                        <div className='d-flex px-3 align-items-center nav'>
                            <h5 className='text-light'>Hey!</h5>
                        </div>
                        <div data-toggle="modal" data-target="#exampleModal" className='d-flex align-items-center justify-content-center sign-out-button'>Login / Register</div>
                    </>
                }
                <div className='side-bar-wrap'>
                    <div>Notifications</div>
                </div>
                <Link to={this.props.isUser ? "/myprofile/booking-history" : ""} >
                    <div className='side-bar-wrap'>
                        <div>Your Orders</div>
                    </div>
                </Link>
                <div className='side-bar-wrap'>
                    <div>Stream Library</div>
                </div>
                <div className='side-bar-wrap'>
                    <div>Play Credit Card</div>
                </div>
                <div className='side-bar-wrap'>
                    <div>Help & Support</div>
                </div>
                <div className='side-bar-wrap'>
                    <div>Accounts & Settings</div>
                </div>
                <div className='side-bar-wrap'>
                    <div>Rewards</div>
                </div>
                <div className='side-bar-wrap'>
                    <div>Restaurant Discounts</div>
                </div>
                <div className='side-bar-wrap'>
                    <div>Discount Store</div>
                </div>
                <div className='side-bar-wrap'>
                    <div>BookASmile</div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => {
            dispatch(removeUser())
        }
    }
}

export default connect(null, mapDispatchToProps)(UserSidebar);