import React, { Component } from 'react';

import './Errors.css';

export class Errors extends Component {
    render() {
        return (
            <div className='d-flex justify-content-center align-items-center error'>
                <h2>{this.props.errorMessage}</h2>
            </div>
        );
    }
}

export default Errors;