import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './NavBottom.css';

class NavBottom extends Component {
  render() {
    return (
      <div className='w-100 nav-bottom'>
        <div className='w-92 h-100 d-flex justify-content-between align-items-center nav-bottom-contain'>
          <ul className='d-flex text-white justify-content-between aligin-items-center nav-bottom-left-list h-100'>
            <Link to="/movies"><li>Movies</li></Link>
            <li>Stream</li>
            <Link to="/events"><li>Events</li></Link>
            <li>Plays</li>
            <li>Sports</li>
            <li>Activities</li>
            <li>Buzz</li>
          </ul>

          <ul className='d-flex text-white justify-content-between aligin-items-center nav-bottom-right-list h-100'>
            <li>ListYourShow</li>
            <li>Corporates</li>
            <li>Offers</li>
            <li>GiftCards</li>
          </ul>
        </div>
      </div>
    );
  }
}

export default NavBottom;