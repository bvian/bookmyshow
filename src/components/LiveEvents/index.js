import React, { Component } from 'react';
import { connect } from 'react-redux';

import './LiveEvents.css';

class LiveEvents extends Component {
    render() {
        console.log(this.props.liveEvents);
        return (
            <div className='carousel carousel-style'>
                <div className='d-flex justify-content-between align-items-center my-3'>
                    <h4 className='head'>The Best of Live Events</h4>
                    <p className='see-all'></p>
                </div>
                <div id="liveEventsCarousel" className="carousel slide w-100" data-wrap="false" data-interval="false">
                    <div className="carousel-inner d-flex">
                        <div className="carousel-item active">
                            <div className='d-flex movies-contain'>
                                {
                                    this.props.liveEvents[0].map((movie) => {
                                        return (
                                            <div key={movie.id} className="card" style={{ width: "14em", height: "14em" }}>
                                                <img className="card-img-top" src={movie.image} alt="" style={{ width: '100%', height: '100%' }} />
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                        <div className="carousel-item">
                            <div className='d-flex movies-contain'>
                                {
                                    this.props.liveEvents[1].map((movie) => {
                                        return (
                                            <div key={movie.id} className="card movie-card" style={{ width: "14em", height: "14em" }}>
                                                <img className="card-img-top" src={movie.image} alt="" style={{ width: '100%', height: '100%' }} />
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                    <a className="carousel-control-prev" href="#liveEventsCarousel" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#liveEventsCarousel" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div>

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        liveEvents: state.events.liveEvents
    }
}

export default connect(mapStateToProps)(LiveEvents);