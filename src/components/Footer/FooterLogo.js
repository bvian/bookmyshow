import React, { Component } from 'react';

import './Footer.css';
import logo from '../../assets/images/logo.svg';

class FooterLogo extends Component {
    render() {
        return (
            <div className='w-100 px-2 footer-articles'>
                <div className='d-flex align-items-center footer-logo-line'>
                    <h2></h2>
                    <img src={logo} alt="" />
                    <h2></h2>
                </div>
            </div>
        );
    }
}

export default FooterLogo;