import React, { Component } from 'react';
import { connect } from 'react-redux';

import './Footer.css';

class FooterArticles extends Component {
    render() {
        return (
            <div className='w-100 footer-articles'>
                <div className='p-0 footer-list-contain'>
                    {
                        this.props.allArticles.map((article) => {

                            return (
                                <div key={article.id} className='footer-articles'>
                                    <h6>{article.title}</h6>
                                    <p>{article.description}</p>
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        allArticles: state.events.footerArticles
    }
}

export default connect(mapStateToProps)(FooterArticles);