import React, { Component } from 'react';

import './Footer.css';
import Facebook from '../../assets/images/Facebook';
import Twitter from '../../assets/images/Twitter';
import Instagram from '../../assets/images/Instagram';
import Youtube from '../../assets/images/Youtube';
import Pinterest from '../../assets/images/Pinterest';
import LinkedIn from '../../assets/images/LinkedIn';

export class FooterSocialIcons extends Component {
    render() {
        return (
            <div className='w-100 footer-articles'>
                <div className='p-0 d-flex justify-content-center align-items-center footer-list-contain'>
                    <div className='d-flex justify-content-center footer-social-icons'>
                        <Facebook />
                        <Twitter />
                        <Instagram />
                        <Youtube />
                        <Pinterest />
                        <LinkedIn />
                    </div>
                </div>
                <div className='p-5 d-flex flex-column align-items-center'>
                    <p>Copyright 2023 © Bigtree Entertainment Pvt. Ltd. All Rights Reserved.</p>
                    <p>
                        The content and images used on this site are copyright protected and copyrights vests with the respective owners. The usage of the content and images on this website is intended to promote the works and no endorsement of the artist shall be implied. Unauthorized use is prohibited and punishable by law.
                    </p>
                </div>
            </div>
        );
    }
}

export default FooterSocialIcons;