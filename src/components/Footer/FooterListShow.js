import React, { Component } from 'react'

import './Footer.css';

class FooterListShow extends Component {
    render() {
        return (
            <div className="footer-list-show">
                <div className='d-flex justify-content-between align-items-center footer-list-contain'>
                    <div className='d-flex align-items-center footer-list-container'>
                        <div className='footer-list-icon'>
                            <img src="https://in.bmscdn.com/webin/common/icons/hut.svg" alt="" />
                        </div>
                        <h6>List your Show</h6>
                        <p>Got a show, event, activity or a great experience? Partner with us & get listed on BookMyShow</p>
                    </div>
                    <button>Contact today!</button>
                </div>
            </div>
        )
    }
}

export default FooterListShow;