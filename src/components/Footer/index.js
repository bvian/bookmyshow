import React, { Component } from 'react';

import './Footer.css';
import FooterArticles from './FooterArticles';
import FooterIcons from './FooterIcons';
import FooterListShow from './FooterListShow';
import FooterLogo from './FooterLogo';
import FooterSocialIcons from './FooterSocialIcons';

export class Footer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showFooter: true
        };
    }

    componentDidMount() {
        const url = window.location.href.split('/');
        if (url.length === 8) {
            this.setState({
                showFooter: false
            })
        } else {
            this.setState({
                showFooter: true
            })
        }
    }

    render() {
        return (
            <div className='footer'>
                {this.state.showFooter &&
                    <>
                        <FooterListShow />
                        <FooterIcons />
                        <FooterArticles />
                        <FooterLogo />
                        <FooterSocialIcons />
                    </>
                }
            </div>
        );
    }
}

export default Footer;