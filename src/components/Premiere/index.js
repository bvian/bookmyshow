import React, { Component } from 'react';

import './Premiere.css';

class Premiere extends Component {
    render() {
        return (
            <div className='premiere'>
                <div className='premiere-image'>
                    <img
                        src="https://assets-in.bmscdn.com/discovery-catalog/collections/tr:w-1440,h-120:q-80/premiere-banner-web-collection-202208191200.png"
                        alt=""
                    />
                </div>
            </div>
        );
    }
}

export default Premiere;