import React, { Component } from 'react';

import './AllSearch.css';
import AdvertiseCarousel from '../../components/AdvertiseCarousel';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

export class AllSearch extends Component {
    constructor(props) {
        super(props);

        this.state = {
            filterTopic: ''
        };
    }

    componentDidMount() {
        const urlTopic = window.location.href.split('/')
            .slice(3)
            .join('')
            .replaceAll('-', ' ');
        this.setState({
            filterTopic: urlTopic
        });
    }

    render() {
        console.log(this.state.filterTopic)
        return (
            <>
                <AdvertiseCarousel />
                <div className='my-5 d-flex w-100 all-movies'>
                    <div className='d-flex all-movies-contain'>
                        <div className='filters'>
                            <h3>Filters</h3>
                        </div>
                        <div className='events'>
                            <h3>{this.state.filterTopic.toUpperCase()} </h3>
                            <div className='d-flex flex-wrap my-5 events-container'>
                                {
                                    this.props.allEvents.filter((event) => {
                                        return event.topic.toLowerCase().includes(this.state.filterTopic);
                                    })
                                        .map((event) => {
                                           

                                            return (
                                                <Link key={event.id}>
                                                    <div className="card movie-card" style={{ width: "14em", height: "30em" }}>
                                                        <img className="card-img-top" src={event.image} alt="" style={{ width: '100%', height: '75%' }} />
                                                        <h6 className="card-title">{event.title}</h6>
                                                        <p>{event.type}</p>
                                                    </div>
                                                </Link>
                                            );
                                        })

                                }
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        allEvents: state.events.allMoviesAndEvents
    }
}

export default connect(mapStateToProps)(AllSearch);