import React, { Component } from 'react';
import { connect } from 'react-redux';

import './SportScreen.css';

class SportScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            sport: {},
        }

    }

    getSport = (title) => {
        const sport = this.props.allEvents.find((event) => {
            return event.title.toLowerCase() === title;
        });

        this.setState({
            sport: sport
        });
    }

    componentDidMount() {
        const urlTitle = window.location.href.split('/')
            .slice(4)
            .join('')
            .replaceAll('-', ' ');
        // console.log(urlTitle);
        this.getSport(urlTitle);
    }

    render() {
        const backgroundImageDark = 'linear-gradient(90deg, #1A1A1A 24.97%, #1A1A1A 38.3%, rgba(26, 26, 26, 0.0409746) 97.47%, #1A1A1A 100%), url(' + this.state.sport?.longImage + ')';

        return (
            <div className='w-100 h-100 d-flex flex-column'>
                <div className='w-100 back-image-contain'>
                    <div
                        className='d-flex align-items-center back-image-wrap'
                        style={{ backgroundImage: backgroundImageDark }}

                    >
                        <div className='d-flex align-items-center movie-book-contain w-100'>
                            <div className='d-flex movie-image-contain'>
                                <img src={this.state.sport?.image} alt="" />
                            </div>
                            <div className='movie-details-contain'></div>
                            <div className='d-flex share-button'></div>

                        </div>

                    </div>

                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        allEvents: state.events.allsportsAndEvents
    }
}

export default connect(mapStateToProps)(SportScreen);