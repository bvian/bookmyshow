import React, { Component } from 'react';
import { connect } from 'react-redux';

import './MovieScreen.css';
import LanguagePopUp from '../../components/LanguagePopUp';

class MovieScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            movieTitle: ""
        }

    }

    getMovie = () => {
        const movie = this.props.movies.find((movie) => {
            const currentTitle = movie.title.replaceAll('-', '')
                .replaceAll('  ', ' ')
                .toLowerCase();
            return currentTitle === this.state.movieTitle;
        });

        return movie;
    }

    componentDidMount() {
        const urlTitle = window.location.href.split('/')
            .slice(4)
            .join('')
            .replaceAll('-', ' ');

        this.setState({
            movieTitle: urlTitle
        });
    }

    render() {
        let movie = this.getMovie();
        movie = movie ? movie : {};
        const backgroundImageDark = 'linear-gradient(90deg, #1A1A1A 24.97%, #1A1A1A 38.3%, rgba(26, 26, 26, 0.0409746) 97.47%, #1A1A1A 100%), url(' + movie?.backgroundImage + ')';

        return (
            <div className='w-100 h-100 d-flex flex-column'>
                <div className='w-100 back-image-contain'>
                    <div
                        className='d-flex align-items-center back-image-wrap'
                        style={{ backgroundImage: backgroundImageDark }}

                    >
                        <div className='d-flex align-items-center movie-book-contain w-100'>
                            <div className='d-flex movie-image-contain'>
                                <img src={movie?.image} alt="" />
                            </div>
                            <div className='d-flex flex-column movie-details-contain'>
                                <h2>{movie?.title}</h2>
                                {movie?.rating?.rate &&
                                    <div className='d-flex align-items-center'>
                                        <h4 className='py-2'>
                                            ⭐
                                            {movie?.rating.rate}/10
                                        </h4>
                                        <h6 className='px-2'>{movie.rating.count}</h6>
                                    </div>
                                }
                                {movie?.rating?.rate === undefined && movie?.rating?.count &&
                                    <div className='d-flex align-items-center'>
                                        <h4 className='py-2'>
                                            👍
                                            {movie.rating.count}
                                        </h4>
                                        <h6 className='px-2'>are interested</h6>
                                    </div>
                                }
                                <div className='d-flex' style={{ gap: "1em" }}>
                                    {/* <div className='screen-shadow'>2D</div> */}
                                    {movie?.language?.map((lang) => {
                                        return <div key={lang} className='screen-shadow'>{lang}</div>
                                    })
                                    }
                                </div>
                                <h5>{movie?.type}</h5>
                                <button className='book-tickets' data-toggle="modal" data-target="#languageModal">Book tickets</button>
                                <LanguagePopUp movie={movie} />
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        movies: state.movies.list
    }
}

export default connect(mapStateToProps)(MovieScreen);