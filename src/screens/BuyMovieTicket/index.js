import React, { Component } from 'react';
import { connect } from 'react-redux';
import Cinemas from '../../components/Cinemas'

import './BuyMovieTicket.css';

class BuyMovieTicket extends Component {
  constructor(props) {
    super(props);

    this.state = {
      movieTitle: "",
      language: ""
    };
  }

  getMovie = () => {
    const movie = this.props.movies.find((currentMovie) => {
      const currentTitle = currentMovie.title
        .replaceAll('-', '')
        .replaceAll('  ', ' ')
        .toLowerCase();
      return currentTitle === this.state.movieTitle;
    });

    return movie;
  }

  filterCinema = () => {
    const cinemas = this.props.cinemas.filter((cinema) => {
      const getCinema = Object.keys(cinema.moviesTime).find((movieTitle) => {
        const currentTitle = movieTitle.replaceAll('-', '')
          .replaceAll('  ', ' ')
          .toLowerCase();

        return currentTitle === this.state.movieTitle;
      });

      return getCinema ? true : false;
    });

    return cinemas;
  }

  componentDidMount() {

    let [movieTitle, language] = window.location.href.split('/').slice(4);
    movieTitle = movieTitle.replaceAll('-', ' ');

    this.setState({
      movieTitle,
      language
    });
  }

  render() {
    const { movieTitle, language } = this.state;
    const movie = this.getMovie();
    const cinemas = this.filterCinema();

    return (
      <div className='w-100'>
        <div className='w-100 buy-movie-ticket-nav'>
          <div className='movie-ticket-nav'>
            <h1>{movieTitle[0]?.toUpperCase() + movieTitle.slice(1)} - {language[0]?.toUpperCase() + language.slice(1)}</h1>
            <div className='d-flex movies-type-wrap'>
              {
                movie?.type.split(',')
                  .map((curType) => {
                    return (
                      <span key={curType}>
                        {curType}
                      </span>
                    );
                  })
              }
            </div>
          </div>
        </div>
        <div className='w-100 cinema-filter-wrap'>

        </div>
        <div className='w-100 current-movie-cinemas'>
          <div className='d-flex flex-column current-movie-cinemas-container'>
            {
              cinemas?.map((cinema) => {
                return (
                  <Cinemas key={cinema.id} movieTitle={movie?.title} cinema={cinema} />
                )
              })
            }
          </div>
          <div className='current-movie-bottom-text'>
            <h6>Privacy Note</h6>
            <p>
              By using www.bookmyshow.com(our website), you are fully accepting the Privacy Policy available at https://bookmyshow.com/privacy governing your access to Bookmyshow and provision of services by Bookmyshow to you. If you do not accept terms mentioned in the Privacy Policy, you must not share any of your personal information and immediately exit Bookmyshow.
            </p>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    movies: state.movies.list,
    cinemas: state.movies.cinemas,
  }
}


export default connect(mapStateToProps)(BuyMovieTicket);